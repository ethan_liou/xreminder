package com.trendmicro.xreminder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class AddEventActivity extends MapActivity implements
		View.OnClickListener {
	Context mContext;
	private final int REQ_CAMERA = 5566;
	AlertDialog mTmpDialog;
	private boolean mHasImage;
	XRemind mReminder;
	String mTmpStr = "";
	ArrayList<PlugIn> mPlugIns;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_event);
		mContext = this;
		mReminder = new XRemind();
		findButtonByParentId(R.id.addevent_people).setOnClickListener(this);
		findButtonByParentId(R.id.addevent_place).setOnClickListener(this);
		findButtonByParentId(R.id.addevent_time).setOnClickListener(this);
		findButtonByParentId(R.id.addevent_event).setOnClickListener(this);
		((TextView) (findViewById(R.id.addevent_people)
				.findViewById(R.id.actionbtn_text))).setText("Who");
		((ImageView) (findViewById(R.id.addevent_people)
				.findViewById(R.id.actionbtn_icon)))
				.setImageResource(R.drawable.people);
		((TextView) (findViewById(R.id.addevent_event)
				.findViewById(R.id.actionbtn_text))).setText("How");
		((ImageView) (findViewById(R.id.addevent_event)
				.findViewById(R.id.actionbtn_icon)))
				.setImageResource(R.drawable.event);

		((TextView) (findViewById(R.id.addevent_time)
				.findViewById(R.id.actionbtn_text))).setText("When");
		((ImageView) (findViewById(R.id.addevent_time)
				.findViewById(R.id.actionbtn_icon)))
				.setImageResource(R.drawable.time);

		((TextView) (findViewById(R.id.addevent_place)
				.findViewById(R.id.actionbtn_text))).setText("Where");
		((ImageView) (findViewById(R.id.addevent_place)
				.findViewById(R.id.actionbtn_icon)))
				.setImageResource(R.drawable.map);
		renderView(R.id.addevent_people,
				new Pair<String, String>("self", null));
		mReminder.mOwner = UserInfo.getUserInfo(this).mPhone;
	}
	
	

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mPlugIns = PlugIn.LoadAllPluginInDB(this);
	}



	private void renderView(int id, Object obj) {
		View v = findViewById(id);
		switch (id) {
		case R.id.addevent_people: {
			Pair<String, String> pair = (Pair<String, String>) obj;
			((TextView) v.findViewById(R.id.actionbtn_detail_1))
					.setText(pair.first);
			if (pair.second != null) {
				((TextView) v.findViewById(R.id.actionbtn_detail_2))
						.setText(pair.second);
			} else {
				((TextView) v.findViewById(R.id.actionbtn_detail_2))
						.setText("");
			}
			break;
		}
		case R.id.addevent_place: {
			Pair<String, String[]> pair = (Pair<String, String[]>) obj;
			String desc = "To" + pair.second[0] + pair.second[1];
			mTmpStr = desc;
			((TextView) v.findViewById(R.id.actionbtn_detail_1))
					.setText(pair.first);
			((TextView) v.findViewById(R.id.actionbtn_detail_2)).setText(desc);
			break;
		}
		case R.id.addevent_event: {
			Pair<String, String[]> pair = (Pair<String, String[]>) obj;
			if (pair.first.equalsIgnoreCase("bus")){
				String desc = pair.second[2] + "minutes before " + pair.second[0] + " arriving " + pair.second[1];
				mTmpStr = desc;
				((TextView) v.findViewById(R.id.actionbtn_detail_1))
						.setText(pair.first);
				((TextView) v.findViewById(R.id.actionbtn_detail_2)).setText(desc);
			}
			else if(pair.first.equalsIgnoreCase("calling")){
				// 電話
				String desc = pair.second[0];
				mTmpStr = desc;
				((TextView) v.findViewById(R.id.actionbtn_detail_1))
						.setText(pair.first);
				((TextView) v.findViewById(R.id.actionbtn_detail_2)).setText(desc);
			}
			else if(pair.first.equalsIgnoreCase("stock")){
				String desc = pair.second[0] + " " + pair.second[1] + " " + pair.second[2];
				mTmpStr = desc;
				((TextView) v.findViewById(R.id.actionbtn_detail_1))
						.setText(pair.first);
				((TextView) v.findViewById(R.id.actionbtn_detail_2)).setText(desc);
			}
			break;
		}
		case R.id.addevent_time: {
			Pair<String, String> pair = (Pair<String, String>) obj;
			((TextView) v.findViewById(R.id.actionbtn_detail_1))
					.setText(pair.first);
			if (pair.second != null) {
				((TextView) v.findViewById(R.id.actionbtn_detail_2))
						.setText(pair.second);
			} else {
				((TextView) v.findViewById(R.id.actionbtn_detail_2))
						.setText("");
			}
			break;
		}
		}
	}

	private View findButtonByParentId(int id) {
		View v = findViewById(id).findViewById(R.id.actionbtn_btn);
		v.setTag(id);
		return v;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch ((Integer) v.getTag()) {
		case R.id.addevent_people:
			showDialog("Select people you want to notify", R.layout.people_dialog, new ActionInterface() {

				public void showAction(final View v) {
					// TODO Auto-generated method stub
					Spinner sp = (Spinner) v
							.findViewById(R.id.peopledlg_category);
					ArrayAdapter<String> sa = new ArrayAdapter<String>(
							mContext, android.R.layout.simple_spinner_item,
							new String[] { "self", "friends" });
					sa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					sp.setAdapter(sa);
					sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

						public void onItemSelected(AdapterView<?> arg0,
								View arg1, int arg2, long arg3) {
							// TODO Auto-generated method stub
							switch (arg2) {
							case 0:
								// 自己
								v.findViewById(R.id.peopledlg_selector)
										.setVisibility(View.GONE);
								break;
							case 1:
								// 朋友
								ListView friend_list = (ListView) v
										.findViewById(R.id.peopledlg_selector);
								friend_list.setVisibility(View.VISIBLE);
								ArrayAdapter<String> la = new ArrayAdapter<String>(
										mContext,
										android.R.layout.simple_list_item_checked,
										new String[] { "Ethan", "Sophia",
												"Chiyi", "Jackey", "Alan",
												"Blacat", "David", "Gina",
												"Frank", "Henry", "Ivan",
												"Jerry" });
								friend_list.setAdapter(la);
								friend_list
										.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
								break;
							default:
								break;
							}
						}

						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub

						}
					});
				}

				public void doneAction(View v) {
					// TODO Auto-generated method stub
					Spinner sp = (Spinner) v
							.findViewById(R.id.peopledlg_category);
					if (sp.getSelectedItemPosition() == 0) {
						// 自己
						renderView(R.id.addevent_people,
								new Pair<String, String>("self", null));
						mReminder.mOwner = UserInfo.getUserInfo(AddEventActivity.this).mPhone;
					} else {
						ListView friend_list = (ListView) v
								.findViewById(R.id.peopledlg_selector);
						int checked_cnt = friend_list.getCheckedItemCount();
						if (checked_cnt == 0) {
							Toast.makeText(mContext, "Please select at least one friend",
									Toast.LENGTH_SHORT).show();
						}
						SparseBooleanArray sba = friend_list
								.getCheckedItemPositions();
						String checked_friend = "";
						for (int i = 0; i < friend_list.getCount(); i++) {
							if (sba.get(i)) {
								checked_friend = checked_friend
										+ friend_list.getItemAtPosition(i)
										+ ",";
							}
						}
						checked_friend = checked_friend.substring(0,
								checked_friend.length() - 1);
						mReminder.mOwner = "096";
						renderView(R.id.addevent_people,
								new Pair<String, String>("Friends", checked_friend));
					}
				}
			});
			break;
		case R.id.addevent_event:
			showDialog("Please select an event", R.layout.event_dialog, new ActionInterface() {
				public void showAction(final View v) {
					// TODO Auto-generated method stub
					Spinner sp = (Spinner) v
							.findViewById(R.id.eventdlg_category);
					final ArrayList<String> plugin_names = new ArrayList<String>();
					for(PlugIn p : mPlugIns){
						if(p.mEnabled)
							plugin_names.add(p.mName);
					}
					ArrayAdapter<String> sa = new ArrayAdapter<String>(
							mContext, android.R.layout.simple_spinner_item,plugin_names);
					sa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					sp.setAdapter(sa);
					sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
						public void onItemSelected(AdapterView<?> arg0,
								View arg1, int arg2, long arg3) {
							// TODO Auto-generated method stub
							if (plugin_names.get(arg2).equalsIgnoreCase("Bus")) {
								// 公車
								final BusHelper bh = new BusHelper(mContext);
								bh.loadFromDB();
								LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								LinearLayout ll = (LinearLayout) v
										.findViewById(R.id.eventdlg_ll);
								ll.removeAllViews();
								final LinearLayout bus = (LinearLayout) inflater
										.inflate(R.layout.bus_dialog, ll);
								Spinner bus_min = (Spinner) bus
										.findViewById(R.id.bus_min);
								ArrayAdapter<String> ma = new ArrayAdapter<String>(
										mContext,
										android.R.layout.simple_spinner_item,
										new String[] { "1", "2", "3", "4", "5",
												"10", "15", "20" });
								ma.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
								bus_min.setAdapter(ma);
								ArrayAdapter<String> ba = new ArrayAdapter<String>(
										mContext,
										android.R.layout.simple_spinner_item,
										bh.getBusList());
								ba.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
								Spinner bus_number = (Spinner) bus
										.findViewById(R.id.bus_number);
								bus_number.setAdapter(ba);
								bus_number
										.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

											public void onItemSelected(
													AdapterView<?> arg0,
													View arg1, int arg2,
													long arg3) {
												// TODO Auto-generated method
												// stub
												String route = (String) arg0
														.getItemAtPosition(arg2);
												ArrayAdapter<String> ba = new ArrayAdapter<String>(
														mContext,
														android.R.layout.simple_spinner_item,
														bh.getStopList(route));
												ba.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
												Spinner bus_stop = (Spinner) bus
														.findViewById(R.id.bus_stop);
												bus_stop.setAdapter(ba);
											}

											public void onNothingSelected(
													AdapterView<?> arg0) {
												// TODO Auto-generated method
												// stub

											}
										});
							}
							else if(plugin_names.get(arg2).equalsIgnoreCase("Phone")){
								// 電話
								LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								LinearLayout ll = (LinearLayout) v
										.findViewById(R.id.eventdlg_ll);
								ll.removeAllViews();
								LinearLayout call = (LinearLayout) inflater
										.inflate(R.layout.call_dialog, (ViewGroup) ll);
							}
							else if(plugin_names.get(arg2).equalsIgnoreCase("Stock")){
								LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								LinearLayout ll = (LinearLayout) v
										.findViewById(R.id.eventdlg_ll);
								ll.removeAllViews();
								LinearLayout stock = (LinearLayout) inflater
										.inflate(R.layout.stock_dialog, (ViewGroup) ll);
								ArrayList<String> stocks = StockPlugIn.getStockLists(mContext);
								ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,  
										  android.R.layout.simple_list_item_1, stocks);  
								AutoCompleteTextView textView = (AutoCompleteTextView)stock.findViewById(R.id.stock_tv);
								textView.setAdapter(adapter); 
							}
							else{
								LinearLayout ll = (LinearLayout) v
										.findViewById(R.id.eventdlg_ll);
								ll.removeAllViews();
							}
						}

						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub

						}
					});
				}

				public void doneAction(View v) {
					// TODO Auto-generated method stub
					Spinner sp = (Spinner) v
							.findViewById(R.id.eventdlg_category);
					final ArrayList<String> plugin_names = new ArrayList<String>();
					for(PlugIn p : mPlugIns){
						if(p.mEnabled)
							plugin_names.add(p.mName);
					}
					
					if(((String)sp.getSelectedItem()).equalsIgnoreCase("bus")){
						// bus
						Spinner sbn = (Spinner) v.findViewById(R.id.bus_number);
						Spinner sbs = (Spinner) v.findViewById(R.id.bus_stop);
						Spinner sbm = (Spinner) v.findViewById(R.id.bus_min);
						Switch direction = (Switch) v
								.findViewById(R.id.bus_go_or_back);
						String route = (String) sbn.getItemAtPosition(sbn
								.getSelectedItemPosition());
						String stop = (String) sbs.getItemAtPosition(sbs
								.getSelectedItemPosition());
						String min_str = (String) sbm.getItemAtPosition(sbm
								.getSelectedItemPosition());
						Integer min = Integer.parseInt(min_str);
						boolean dir = direction.isChecked();
						XEvent event = new XEvent(XEvent.EVENT_BUS, -1);
						XBusService bs = new XBusService(route, dir, stop, min);
						event.mObj = bs;
						mReminder.mEvent = event;
						renderView(R.id.addevent_event, new Pair<String, String[]>(
								"Bus", new String[] { route, stop, min_str }));
					}
					else if(((String)sp.getSelectedItem()).equalsIgnoreCase("phone")){
						// call
						EditText et = (EditText) v.findViewById(R.id.calldlg_phone);
						XEvent event = new XEvent(XEvent.EVENT_CALL, -1);
						XCallService cs = new XCallService(et.getText().toString());
						event.mObj = cs;
						mReminder.mEvent = event;
						renderView(R.id.addevent_event, new Pair<String, String[]>(
								"Calling", new String[] { et.getText().toString() }));

					}
					else if(((String)sp.getSelectedItem()).equalsIgnoreCase("stock")){
						EditText et = (EditText) v.findViewById(R.id.stock_value);
						AutoCompleteTextView actv = (AutoCompleteTextView) v.findViewById(R.id.stock_tv);
						Switch dir = (Switch) v.findViewById(R.id.stock_direction);
						XEvent event = new XEvent(XEvent.EVENT_STOCK, -1);
						XStockService xss = new XStockService();
						xss.mDirection = dir.isChecked();
						xss.mValue = Float.parseFloat(et.getText().toString());
						String stock = actv.getText().toString();
						xss.mNo = Integer.parseInt(stock.split(":")[0]);
						xss.mName = stock.split(":")[1];
						event.mObj = xss;
						mReminder.mEvent = event;
						renderView(R.id.addevent_event, new Pair<String, String[]>(
								"Stock", new String[] { stock, xss.mDirection ? ">" : "<", ""+xss.mValue}));
					}

				}
			});
			break;
		case R.id.addevent_time:
			showDialog("test", R.layout.time_dialog, new ActionInterface() {

				public void showAction(View v) {
					// TODO Auto-generated method stub

				}

				public void doneAction(View v) {
					// TODO Auto-generated method stub
					final EditText et = (EditText) v.findViewById(R.id.time_et);
					et.setVisibility(View.VISIBLE);
					renderView(R.id.addevent_time, new Pair<String, String>(
							"Time", et.getText().toString()));
				}
			});
			break;
		case R.id.addevent_place:
			showDialog("Please select a location", R.layout.location_dialog,
					new ActionInterface() {

						public void showAction(View v) {
							// TODO Auto-generated method stub
							Spinner cat = (Spinner) v
									.findViewById(R.id.location_category);
							ArrayAdapter<String> sa = new ArrayAdapter<String>(
									mContext,
									android.R.layout.simple_spinner_item,
									new String[] { "Map", "Store" });
							sa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							cat.setAdapter(sa);
							final MapView mv = (MapView) v
									.findViewById(R.id.location_map);
							final LinearLayout ll = (LinearLayout) v
									.findViewById(R.id.location_bigstore);
							cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

								public void onItemSelected(AdapterView<?> arg0,
										View arg1, int arg2, long arg3) {
									// TODO Auto-generated method stub
									if (arg2 == 0) {
										// map
										mv.setVisibility(View.VISIBLE);
										ll.setVisibility(View.GONE);
										mv.getController().setZoom(17);
										mv.getController().setCenter(
												new GeoPoint(25023648,
														121546447));
									} else {
										// store
										mv.setVisibility(View.GONE);
										ll.setVisibility(View.VISIBLE);
										Spinner store = (Spinner) ll
												.findViewById(R.id.location_store);
										Spinner location = (Spinner) ll
												.findViewById(R.id.location_location);
										store.setAdapter(createAdapter(new String[] {
												"家樂福", "大潤發" }));
										location.setAdapter(createAdapter(new String[] {
												"台北店", "台中店", "高雄店" }));
									}
								}

								public void onNothingSelected(
										AdapterView<?> arg0) {
									// TODO Auto-generated method stub

								}
							});
						}

						public void doneAction(View v) {
							// TODO Auto-generated method stub
							final LinearLayout ll = (LinearLayout) v
									.findViewById(R.id.location_bigstore);
							ll.setVisibility(View.VISIBLE);
							Spinner store = (Spinner) ll
									.findViewById(R.id.location_store);
							Spinner location = (Spinner) ll
									.findViewById(R.id.location_location);
							renderView(
									R.id.addevent_place,
									new Pair<String, String[]>(
											"賣場",
											new String[] {
													(String) store
															.getSelectedItem(),
													(String) location
															.getSelectedItem() }));
						}
					});
			break;
		default:
			break;
		}
	}

	private ArrayAdapter<String> createAdapter(String[] strs) {
		ArrayAdapter<String> arr = new ArrayAdapter<String>(mContext,
				android.R.layout.simple_spinner_item, strs);
		arr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		return arr;
	}

	private interface ActionInterface {
		public void doneAction(View v);

		public void showAction(View v);
	}

	void showDialog(String title, int resID, final ActionInterface action) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		final LinearLayout dlg = (LinearLayout) inflater.inflate(resID, null);
		if (action != null) {
			action.showAction(dlg);
		}
		mTmpDialog = new AlertDialog.Builder(this)
				.setTitle(title)
				.setView(dlg)
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.cancel();
							}
						})
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (action != null) {
							action.doneAction(dlg);
						}
						dialog.cancel();
					}
				}).show();
		mTmpDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		mTmpDialog
				.setOnDismissListener(new DialogInterface.OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						ViewGroup vg = (ViewGroup) dlg.getParent();
						vg.removeView(dlg);
						mTmpDialog = null;
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.addevent_menu, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQ_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				mHasImage = true;
				Bundle extras = data.getExtras();
				Bitmap bitmap = (Bitmap) extras.get("data");
				try {
					FileOutputStream fos = new FileOutputStream(new File(
							mContext.getCacheDir(), "tmp.jpg"));
					bitmap.compress(CompressFormat.JPEG, 90, fos);
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (mTmpDialog != null) {
					ImageButton iv = (ImageButton) mTmpDialog
							.findViewById(R.id.trigger_img);
					iv.setImageBitmap(bitmap);
				}
			}
			break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.menu_accept) {
			mHasImage = false;
			showDialog("Please input your notification", R.layout.trigger_act, new ActionInterface() {

				public void showAction(View v) {
					// TODO Auto-generated method stub
					TextView tv = (TextView) v.findViewById(R.id.trigger_txt);
					tv.setText(mTmpStr);
					ImageButton camera_btn = (ImageButton) v
							.findViewById(R.id.trigger_img);
					camera_btn.setOnClickListener(new View.OnClickListener() {

						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent cameraIntent = new Intent(
									MediaStore.ACTION_IMAGE_CAPTURE);
							startActivityForResult(cameraIntent, REQ_CAMERA);
						}
					});
				}

				public void doneAction(View v) {
					// TODO Auto-generated method stub
					TextView tv = (TextView) v.findViewById(R.id.trigger_txt);
					UserInfo info = UserInfo.getUserInfo(mContext);
					mReminder.mCreator = info.mPhone;
					mReminder.mRemindText = tv.getText().toString();
					mReminder.saveAll(mContext);
					new Thread(new Runnable() {
						
						public void run() {
							// TODO Auto-generated method stub
							ServerAPI.SetReminder(mReminder);							
						}
					}).start();
					Intent intent = new Intent(mContext, AlarmDialog.class);

					if (mHasImage) {
						try {
							File f = new File(getFilesDir(), "image"
									+ mReminder.mId);
							FileOutputStream fos = new FileOutputStream(f);
							FileInputStream fis = new FileInputStream(new File(
									getCacheDir(), "tmp.jpg"));
							byte[] b = new byte[1024];
							while (fis.read(b) > 0) {
								fos.write(b);
							}
							fos.flush();
							fos.close();
							fis.close();
							intent.putExtra("image", f.getAbsolutePath());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
//					intent.putExtra("title", "大潤發到囉");
//					intent.putExtra("message", mReminder.mRemindText);
//					AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
//					PendingIntent pi = PendingIntent.getActivity(mContext, 123,
//							intent, 0);
//					am.set(AlarmManager.RTC_WAKEUP,
//							System.currentTimeMillis() + 10 * 1000, pi);

					setResult(RESULT_OK);
					finish();
				}
			});
		} else if (item.getItemId() == R.id.menu_cancel) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
}
