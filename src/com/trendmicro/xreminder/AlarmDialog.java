package com.trendmicro.xreminder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AlarmDialog extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
		v.vibrate(500);
		setContentView(R.layout.alarm_dialog);
		LinearLayout ll = (LinearLayout) findViewById(R.id.alarmdlg_ll);
		LayoutInflater li = LayoutInflater.from(this);
		LinearLayout content = (LinearLayout) li.inflate(
				R.layout.alarm_dialog_content, null);
		
		Intent intent = getIntent();
		// hard code
		if (intent.hasExtra("image")) {
			ImageView iv = (ImageView) content
					.findViewById(R.id.alarmdlgcnt_iv);
			iv.setImageBitmap(BitmapFactory.decodeFile(intent
					.getStringExtra("image")));
		}
		String title = intent.getStringExtra("title");
		TextView tv = (TextView) content.findViewById(R.id.alarmdlgcnt_tv);
		tv.setText(intent.getStringExtra("message"));

		AlertDialog.Builder builder = new AlertDialog.Builder(this,AlertDialog.THEME_HOLO_DARK)
				.setTitle(title)
				.setView(content)
				.setPositiveButton("Got it!",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
					}
				})
				.setNegativeButton("Finish it!", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
					}
				});
		AlertDialog dialog = builder.create();
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    WindowManager.LayoutParams WMLP = dialog.getWindow().getAttributes();
	    WMLP.gravity = Gravity.TOP | Gravity.LEFT;
	    WMLP.y = 400;   //y position
		dialog.show();
		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			public void onDismiss(DialogInterface arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});		
	}
}
