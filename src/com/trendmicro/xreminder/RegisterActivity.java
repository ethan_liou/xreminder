package com.trendmicro.xreminder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reg_act);
		final EditText phone_et = (EditText) findViewById(R.id.reg_phone);
		Button btn = (Button)findViewById(R.id.reg_btn);
		btn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UserInfo info = UserInfo.getUserInfo(RegisterActivity.this);
				info.mPhone = phone_et.getText().toString();
				info.writeUserInfo(RegisterActivity.this);
				final ProgressDialog pd = ProgressDialog.show(RegisterActivity.this, "Loading...", "Please wait...");
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						ServerAPI.GetReminders(phone_et.getText().toString(), false);
						
						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								pd.dismiss();
								finish();
							}
						});
					}
				}).start();
			}
		});
	}
}
