package com.trendmicro.xreminder;

import java.util.ArrayList;
import java.util.Random;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class PlugIn {
	int mId;
	String mName;
	String mDescription;
	String mIconName;
	float mPrice;
	boolean mEnabled;
	ContentValues mContents;
		
	public PlugIn(){
		super();
	}
	
	public PlugIn(int mId, String mName, String mDescription, String mIconName,
			float mPrice, boolean mEnabled) {
		super();
		this.mId = mId;
		this.mName = mName;
		this.mDescription = mDescription;
		this.mIconName = mIconName;
		this.mPrice = mPrice;
		this.mEnabled = mEnabled;
	}
	
	public static ArrayList<PlugIn> LoadAllPluginInDB(Context context){
		ArrayList<PlugIn> plugins = new ArrayList<PlugIn>();
		DBHelper helper = new DBHelper(context, DBHelper.DB_TYPE.PLUGIN);
		Cursor cs = helper.getReadableDatabase().query("PLUGIN", null, null,
				null, null, null, null);
		while (cs.moveToNext()) {
			PlugIn plugin = new PlugIn();
			plugin.mId = cs.getInt(cs.getColumnIndex("ID"));
			plugin.mName = cs.getString(cs.getColumnIndex("Name"));
			plugin.mDescription = cs.getString(cs.getColumnIndex("Description"));
			plugin.mPrice = cs.getFloat(cs.getColumnIndex("Price"));
			plugin.mEnabled = cs.getInt(cs.getColumnIndex("Enabled")) == 1;
			plugin.mIconName = cs.getString(cs.getColumnIndex("IconName"));		
			plugins.add(plugin);
		}
		cs.close();
		helper.close();
		return plugins;
	}
	
	private void setContentValues(){
		mContents = new ContentValues();
		mContents.put("Name", mName);
		mContents.put("Description", mDescription);
		mContents.put("Price", mPrice);
		mContents.put("Enabled", mEnabled);
		mContents.put("IconName", mIconName);	
	}

	public void saveToDB(Context mContext){
		DBHelper helper = new DBHelper(mContext,DBHelper.DB_TYPE.PLUGIN);
		setContentValues();
		mId = (int) helper.getWritableDatabase().insert("PLUGIN", null, mContents);
		helper.close();
	}
	
	public void updateDB(Context context){
		DBHelper helper = new DBHelper(context,DBHelper.DB_TYPE.PLUGIN);
		setContentValues();
		helper.getWritableDatabase().update("PLUGIN", mContents, "ID=?", new String[]{""+mId});
		helper.close();
	}
}
