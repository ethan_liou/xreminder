package com.trendmicro.xreminder;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class XRemind extends XObject{
	String mOwner;
	String mCreator;
	String mRemindText;
	XEvent mEvent;
	XTime mTime;
	XLocation mLocation;
	boolean mFinished;

	static public ArrayList<XRemind> loadFromDB(Context context) {
		ArrayList<XRemind> remindList = new ArrayList<XRemind>();
		DBHelper helper = new DBHelper(context, DBHelper.DB_TYPE.REMINDER);
		Cursor cs = helper.getReadableDatabase().query("REMINDER", null, null,
				null, null, null, null);
		String my_phone = UserInfo.getUserInfo(context).mPhone;
		while (cs.moveToNext()) {
			XRemind remind = new XRemind();
			String owner = cs.getString(cs.getColumnIndex("Owner"));
			if (!owner.equals(my_phone)){
				continue;
			}
			remind.mId = cs.getInt(cs.getColumnIndex("ID"));
			remind.mFinished = cs.getInt(cs.getColumnIndex("Finished")) == 1;
			remind.mRemindText = cs.getString(cs.getColumnIndex("RemindText"));
			remindList.add(remind);
		}
		cs.close();
		helper.close();
		return remindList;
	}
	
	public XRemind(){
		super();
	}
	
	public XRemind(String mOwner, String mCreator, XEvent mEvent,
			XTime mTime, XLocation mLocation, String mRemindText, boolean mFinished) {
		super();
		this.mOwner = mOwner;
		this.mCreator = mCreator;
		this.mEvent = mEvent;
		this.mTime = mTime;
		this.mLocation = mLocation;
		this.mRemindText = mRemindText;
		this.mFinished = mFinished;
	}
	
	@Override
	public void saveToDB(Context mContext) {
		// TODO Auto-generated method stub
		super.saveToDB(mContext);
	}

	public void saveAll(Context context){
		if(mEvent != null){
			mEvent.saveToDB(context);
		}
		if(mTime != null){
			mTime.saveToDB(context);
		}
		if(mLocation != null){
			mLocation.saveToDB(context);
		}
		saveToDB(context);
	}
	@Override
	void setTableName() {
		// TODO Auto-generated method stub
		mTableName = "REMINDER";
	}
	@Override
	void setContentValues() {
		// TODO Auto-generated method stub
		mContents = new ContentValues();
		mContents.put("Owner", mOwner);
		mContents.put("Creator", mCreator);
		mContents.put("EventID", mEvent == null ? -1 : mEvent.mId);
		mContents.put("TimeID", mTime == null ? -1 : mTime.mId);
		mContents.put("LocationID", mLocation == null ? -1 : mLocation.mId);
		mContents.put("RemindText", mRemindText);
		mContents.put("Finished", mFinished);
	}
	
	@Override
	String toJsonString() {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		try {
			obj.put("owner", mOwner);
			obj.put("creator", mCreator);
			obj.put("remind_text", mRemindText);
			if(mTime != null)
				obj.put("time", mTime.toJsonString());
			if(mLocation != null)
				obj.put("location", mLocation.toJsonString());
			if(mEvent != null)
				obj.put("event", mEvent.toJsonString());
			obj.put("finished", mFinished);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj.toString();
	}

	@Override
	void fromJsonString(String jsonString) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(jsonString);
			mId = -1;
			mOwner = obj.getString("owner");
			mCreator = obj.getString("creator");
			mRemindText = obj.getString("remind_text");
			if(obj.has("time")){
				mTime = new XTime();
				mTime.fromJsonString(obj.getString("time"));
			}
			if(obj.has("location")){
				mLocation = new XLocation();
				mLocation.fromJsonString(obj.getString("location"));
			}
			if(obj.has("event")){
				mEvent = new XEvent();
				mEvent.fromJsonString(obj.getString("event"));
			}
			mFinished = obj.getBoolean("finished");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
