package com.trendmicro.xreminder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;

public class XStockService extends XObject {
	int mNo;
	String mName;
	boolean mDirection;
	float mValue;
	
	@Override
	String toJsonString() {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		try {
			obj.put("no", mNo);
			obj.put("name", mName);
			obj.put("direction", mDirection);
			obj.put("value", mValue);			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj.toString();
	}

	@Override
	void fromJsonString(String jsonString) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(jsonString);
			mId = -1;
			mNo = obj.getInt("no");
			mName = obj.getString("name");
			mDirection = obj.getBoolean("direction");
			mValue = (float) obj.getDouble("value");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	void setTableName() {
		// TODO Auto-generated method stub
		mTableName = "STOCKSERVICE";
	}

	@Override
	void setContentValues() {
		// TODO Auto-generated method stub
		mContents = new ContentValues();
		mContents.put("No", mNo);
		mContents.put("Name", mName);
		mContents.put("Direction", mDirection);
		mContents.put("Value", mValue);	
	}

}
