package com.trendmicro.xreminder;

import java.util.ArrayList;
import java.util.List;

import com.trendmicro.xreminder.DBHelper.DB_TYPE;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StoreActivity extends Activity {
	
	private ArrayList<PlugIn> loadPlugin(){
		return PlugIn.LoadAllPluginInDB(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		ListView lv = new ListView(this);
		ArrayList<PlugIn> lists = loadPlugin();
		StoreArrayAdapter adapter = new StoreArrayAdapter(this, 0, lists);
		lv.setAdapter(adapter);
		setContentView(lv);
	}
}

class StoreArrayAdapter extends ArrayAdapter<PlugIn>{
	LayoutInflater inflater;
	Context mContext;
	public StoreArrayAdapter(Context context, int textViewResourceId,
			List<PlugIn> objects) {
		super(context, textViewResourceId, objects);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		mContext = context;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		RelativeLayout rl = null;
		if(convertView == null)
			rl = (RelativeLayout) inflater.inflate(R.layout.store_cell, null);
		else rl = (RelativeLayout) convertView;
		
		PlugIn pi = getItem(position);
		
		final Button btn = (Button) rl.findViewById(R.id.store_cell_btn);
		TextView main_title = (TextView) rl.findViewById(R.id.store_cell_main_title);
		TextView second_title = (TextView) rl.findViewById(R.id.store_cell_second_title);
		ImageView icon = (ImageView)rl.findViewById(R.id.store_cell_icon);
		main_title.setText(pi.mName);
		second_title.setText(pi.mDescription);
		Resources r = mContext.getResources();
		int res_id = r.getIdentifier(pi.mIconName, "drawable", mContext.getPackageName());
		icon.setImageResource(res_id);
		btn.setText("$"+pi.mPrice);
		btn.setEnabled(!pi.mEnabled);
		btn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btn.setEnabled(false);
				PlugIn p = getItem(position);
				p.mEnabled = true;
				p.updateDB(mContext);
			}
		});
		return rl;
	}	
}