package com.trendmicro.xreminder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.util.Pair;

public class BusHelper {
	// map ( route, stop )
	static private HashMap<Integer, ArrayList<String> > stop_map;
	static private HashMap<String, Integer> bus_map;
	private Context mContext;
	public BusHelper(Context context) {
		super();
		mContext = context;
		if(stop_map == null){
			stop_map = new HashMap<Integer, ArrayList<String> >();
		}
		if(bus_map == null){
			bus_map = new HashMap<String, Integer>();
		}
	}
	
	public List<String> getBusList(){
		Set<String> keyset = bus_map.keySet();
		List<String> sort_keys = new ArrayList<String>(keyset);
		Collections.sort(sort_keys);
		return sort_keys;
	}
	
	public List<String> getStopList(String bus_name){
		int id = bus_map.get(bus_name);
		return stop_map.get(id);
	}
	
	public void loadFromDB(){
		if(stop_map.size() != 0 && bus_map.size() != 0)
			return;
		DBHelper helper = new DBHelper(mContext,DBHelper.DB_TYPE.BUS);
		Cursor cursor = helper.getReadableDatabase().query("routes", new String[]{"nameZh","Id"}, null, null, null, null, null);
		while(cursor.moveToNext()){
			String route = cursor.getString(0);
			int id = cursor.getInt(1);
			bus_map.put(route, id);
		}
		cursor.close();
		
		cursor = helper.getReadableDatabase().query("stops", new String[]{"nameZh","routeId"}, null, null, null, null, null);
		while(cursor.moveToNext()){
			String stop = cursor.getString(0);
			int route_id = cursor.getInt(1);
			if(!stop_map.containsKey(route_id)){
				ArrayList<String> stops = new ArrayList<String>();
				stop_map.put(route_id, stops);
			}
			ArrayList<String> stops = stop_map.get(route_id);
			stops.add(stop);
		}
		cursor.close();
		helper.close();
	}
}
