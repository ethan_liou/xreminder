package com.trendmicro.xreminder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	public enum DB_TYPE{
		BUS,
		REMINDER,
		PLUGIN
	}
	private final static String DB_PATH = "/data/data/com.trendmicro.xreminder/databases/";
	private final static String[] DB_NAME = {"5284.db","x_reminder.sqlite","xplugin.sqlite"};
	private final static int[] DB_VERSION = {1,1,1};
	private SQLiteDatabase mDB;
	private Context mContext;
	
	public DBHelper(Context context,DB_TYPE db_type) {
		super(context, DB_NAME[db_type.ordinal()], null, DB_VERSION[db_type.ordinal()]);
		mContext = context;
		createDataBase();
	}

	public void createDataBase() {
		boolean dbExist = checkDataBase();
		if (!dbExist) {
			getReadableDatabase();
			copyDataBase();
		}
	}

	private boolean checkDataBase() {
		SQLiteDatabase checkDB = null;
		try {
			String myPath = DB_PATH + getDatabaseName();
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		if (checkDB != null) {
			checkDB.close();
		}
		return checkDB != null ? true : false;
	}

	private void copyDataBase() {
		try {
			InputStream myInput = mContext.getAssets().open(getDatabaseName());
			String outFileName = DB_PATH + getDatabaseName();
			OutputStream myOutput = new FileOutputStream(outFileName);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput.write(buffer, 0, length);
			}

			myOutput.flush();
			myOutput.close();
			myInput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void openDataBase() {
		String myPath = DB_PATH + getDatabaseName();
		mDB = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.NO_LOCALIZED_COLLATORS);
	}

	@Override
	public synchronized void close() {
		if (mDB != null)
			mDB.close();
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
	}	
}
