package com.trendmicro.xreminder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;

public class XCallService extends XObject {
	String mPhone;
	
	public XCallService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public XCallService(String mPhone) {
		super();
		this.mPhone = mPhone;
	}

	@Override
	void setTableName() {
		// TODO Auto-generated method stub
		mTableName = "CALLSERVICE";
	}

	@Override
	void setContentValues() {
		// TODO Auto-generated method stub
		mContents = new ContentValues();
		mContents.put("Phone", mPhone);
	}

	@Override
	String toJsonString() {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		try {
			obj.put("phone", mPhone);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj.toString();
	}

	@Override
	void fromJsonString(String jsonString) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(jsonString);
			mId = -1;
			mPhone = obj.getString("phone");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
