package com.trendmicro.xreminder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;

public class XLocation extends XObject{
	String mCategory;
	String mArea;
	String mStoreName;
	
	public XLocation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public XLocation(String mCategory, String mArea, String mStoreName) {
		super();
		this.mCategory = mCategory;
		this.mArea = mArea;
		this.mStoreName = mStoreName;
	}
	
	@Override
	public void saveToDB(Context mContext) {
		// TODO Auto-generated method stub
		super.saveToDB(mContext);
	}

	@Override
	void setTableName() {
		// TODO Auto-generated method stub
		mTableName = "LOCATION";
	}

	@Override
	void setContentValues() {
		// TODO Auto-generated method stub
		mContents = new ContentValues();
		mContents.put("Category", mCategory);
		mContents.put("Area", mArea);
		mContents.put("StoreName", mStoreName);
	}

	@Override
	String toJsonString() {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		try {
			obj.put("category", mCategory);
			obj.put("area", mArea);
			obj.put("store_name", mStoreName);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj.toString();
	}

	@Override
	void fromJsonString(String jsonString) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(jsonString);
			mId = -1;
			mCategory = obj.getString("category");
			mArea = obj.getString("area");
			mStoreName = obj.getString("store_name");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
