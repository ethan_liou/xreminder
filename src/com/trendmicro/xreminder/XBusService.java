package com.trendmicro.xreminder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;

public class XBusService extends XObject{
	String mBusNum;
	boolean mDirection;
	String mStopName;
	int mMin;
	
	public XBusService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public XBusService(String mBusNum, boolean mDirection,
			String mStopName, int mMin) {
		super();
		this.mBusNum = mBusNum;
		this.mDirection = mDirection;
		this.mStopName = mStopName;
		this.mMin = mMin;
	}
	
	@Override
	public void saveToDB(Context mContext) {
		// TODO Auto-generated method stub
		super.saveToDB(mContext);
	}

	@Override
	void setTableName() {
		// TODO Auto-generated method stub
		mTableName = "BUSSERVICE";
	}

	@Override
	void setContentValues() {
		// TODO Auto-generated method stub
		mContents = new ContentValues();
		mContents.put("BusNum", mBusNum);
		mContents.put("Direction", mDirection);
		mContents.put("StopName", mStopName);
		mContents.put("Minutes", mMin);
	}

	@Override
	String toJsonString() {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		try {
			obj.put("bus_num", mBusNum);
			obj.put("direction",mDirection);
			obj.put("stop_name", mStopName);
			obj.put("min", mMin);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj.toString();
	}

	@Override
	void fromJsonString(String jsonString) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(jsonString);
			mId = -1;
			mBusNum = obj.getString("bus_num");
			mDirection = obj.getBoolean("direction");
			mStopName = obj.getString("stop_name");
			mMin = obj.getInt("min");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
