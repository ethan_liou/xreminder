package com.trendmicro.xreminder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;

public class XTime extends XObject{
	
	long mStart;
	long mEnd;
	int mCycle;
	
	public XTime() {
		super();
		// TODO Auto-generated constructor stub
	}

	public XTime(long mStart, long mEnd, int mCycle) {
		super();
		this.mStart = mStart;
		this.mEnd = mEnd;
		this.mCycle = mCycle;
	}
	
	@Override
	public void saveToDB(Context mContext) {
		// TODO Auto-generated method stub
		super.saveToDB(mContext);
	}

	@Override
	void setTableName() {
		// TODO Auto-generated method stub
		mTableName = "TIME";
	}

	@Override
	void setContentValues() {
		// TODO Auto-generated method stub
		mContents = new ContentValues();
		mContents.put("Start", mStart);
		mContents.put("End", mEnd);
		mContents.put("Cycle", mCycle);
	}
	
	@Override
	String toJsonString() {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		try {
			obj.put("id",mId);
			obj.put("start", mStart);
			obj.put("end", mEnd);
			obj.put("cycle", mCycle);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj.toString();
	}

	@Override
	void fromJsonString(String jsonString) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(jsonString);
			mId = obj.getInt("id");
			mStart = obj.getLong("start");
			mEnd = obj.getLong("end");
			mCycle = obj.getInt("cycle");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
