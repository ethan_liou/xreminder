package com.trendmicro.xreminder;

import java.util.ArrayList;

import com.trendmicro.xreminder.DBHelper.DB_TYPE;

import android.content.Context;
import android.database.Cursor;
import android.util.Pair;

public class StockPlugIn {
	static ArrayList<String> stocks;
	public static ArrayList<String> getStockLists(Context context){
		if(stocks == null){
			stocks = new ArrayList<String>();
			DBHelper helper = new DBHelper(context, DB_TYPE.PLUGIN);
			Cursor c = helper.getReadableDatabase().query("STOCKLIST", null, null, null, null, null, null);
			while(c.moveToNext()){
				stocks.add(""+c.getString(0)+":"+c.getString(1));
			}
		}
		return stocks;
	}
}
