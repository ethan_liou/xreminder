package com.trendmicro.xreminder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	static boolean thread_start = true;
	ListView lv;
	int ADD_REQ_CODE = 3344;
	int STORE_REQ_CODE = 5566;	
	Thread mFetchThread = new Thread(new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				thread_start = true;
				while(thread_start){
					UserInfo info = UserInfo.getUserInfo(MainActivity.this);
					if(info.mPhone == null)
						continue;
					ArrayList<XRemind> reminders = ServerAPI.GetReminders(info.mPhone, true);
					for(XRemind r : reminders){
						r.saveAll(MainActivity.this);
					}
					if(reminders.size() != 0){
						Log.e("EEE","size != 0");
						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								refresh();
							}
						});
					}
					Thread.sleep(10000);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	});

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		thread_start = false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		UserInfo info = UserInfo.getUserInfo(this);
		if(info.mPhone == ""){
			// register
			startActivity(new Intent(this, RegisterActivity.class));
		}
		mFetchThread.start();
		
		lv = new ListView(this);
		refresh();
		setContentView(lv);
	}
	
	public void refresh(){
		RemindArrayAdapter raa = new RemindArrayAdapter(this,R.layout.event_cell, XRemind.loadFromDB(this));
		lv.setAdapter(raa);		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.menu_add) {
			startActivityForResult(new Intent(getApplicationContext(),
					AddEventActivity.class), ADD_REQ_CODE);
		} else {
			startActivityForResult(new Intent(getApplicationContext(),
					StoreActivity.class), STORE_REQ_CODE);
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onActivityResult(int request_code, int result_code,
			Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(request_code, request_code, arg2);
		Log.e("FQWE", "" + request_code + "," + result_code);
		if (result_code == Activity.RESULT_OK && request_code == ADD_REQ_CODE) {
			refresh();
		}
	}
}
