package com.trendmicro.xreminder;

import android.content.Context;
import android.content.SharedPreferences;

public class UserInfo {
	static UserInfo mUser;
	String mPhone;
	
	final String PREF_NAME = "user_info";
	final String PHONE_KEY = "phone";
	
	public static UserInfo getUserInfo(Context context){
		if(mUser == null){
			mUser = new UserInfo();
			mUser.readUserInfo(context);
		}
		return mUser;
	}
	
	public boolean readUserInfo(Context context){
		SharedPreferences sp = context.getSharedPreferences(PREF_NAME, 0);
		mPhone = sp.getString(PHONE_KEY, "");
		return mPhone != "";
	}
	
	public boolean writeUserInfo(Context context){
		SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, 0).edit();
		editor.putString(PHONE_KEY, mPhone);
		return editor.commit();
	}
}
