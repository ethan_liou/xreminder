package com.trendmicro.xreminder;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

abstract public class XObject {
	public int mId;
	public String mTableName;
	public ContentValues mContents;
	public XObject(){
		super();
		mId = -1;
	}
	
	abstract String toJsonString();
	abstract void fromJsonString(String jsonString);
	
	public void saveToDB(Context mContext){
		DBHelper helper = new DBHelper(mContext,DBHelper.DB_TYPE.REMINDER);
		setTableName();
		setContentValues();
		mId = (int) helper.getWritableDatabase().insert(mTableName, null, mContents);
		helper.close();
	}
	
	public void updateDB(Context context){
		DBHelper helper = new DBHelper(context,DBHelper.DB_TYPE.REMINDER);
		setTableName();
		setContentValues();
		helper.getWritableDatabase().update(mTableName, mContents, "ID=?", new String[]{""+mId});
		helper.close();
	}
	
	abstract void setTableName();
	abstract void setContentValues();
}
