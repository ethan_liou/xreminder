package com.trendmicro.xreminder;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ServerAPI {
	static final String SERVER_URL = "http://thexreminder.appspot.com/";
	static final String SET_ACTION = "set_event";
	static final String GET_ACTION = "get_event";
	
	public static ArrayList<XRemind> GetReminders(String number,boolean deleted){
		ArrayList<XRemind> reminders = new ArrayList<XRemind>();
		try {
			JSONObject obj = new JSONObject();
			obj.put("id", number);
			obj.put("delete", deleted?1:0);
			String json_enc = URLEncoder.encode(obj.toString());
			String url = SERVER_URL + GET_ACTION + "?" + json_enc;
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			String retSrc = EntityUtils.toString(response.getEntity());
			JSONObject ret_obj = new JSONObject(retSrc);
			JSONArray arr = ret_obj.getJSONArray("events");
			for(int i = 0 ; i < arr.length() ; i++){
				XRemind r = new XRemind();
				String json_str = arr.getString(i);
				r.fromJsonString(json_str);
				reminders.add(r);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reminders;
	}
	
	public static boolean SetReminder(XRemind remind){
		try {
			JSONObject obj = new JSONObject();
			obj.put("id", remind.mOwner);
			JSONArray array = new JSONArray();
			JSONObject remind_obj = new JSONObject(remind.toJsonString());
			array.put(remind_obj);
			obj.put("events", array);
			String json_enc = URLEncoder.encode(obj.toString());
			String url = SERVER_URL + SET_ACTION + "?" + json_enc;
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			String retSrc = EntityUtils.toString(response.getEntity());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}	
}
