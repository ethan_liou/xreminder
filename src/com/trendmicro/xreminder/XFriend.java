package com.trendmicro.xreminder;

import android.content.ContentValues;

public class XFriend extends XObject{
	
	String mName;
	String mPhone;
	
	public XFriend(String mName, String mPhone) {
		super();
		this.mName = mName;
		this.mPhone = mPhone;
	}

	public XFriend(){
		super();
	}
	
	@Override
	String toJsonString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	void fromJsonString(String jsonString) {
		// TODO Auto-generated method stub
		
	}

	@Override
	void setTableName() {
		// TODO Auto-generated method stub
		mTableName = "FRIEND";
	}

	@Override
	void setContentValues() {
		// TODO Auto-generated method stub
		mContents = new ContentValues();
		mContents.put("Name", mName);
		mContents.put("Phone", mPhone);
	}
	
}
