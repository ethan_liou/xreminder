package com.trendmicro.xreminder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;

public class XEvent extends XObject {
	public static final int EVENT_BUS = 0;
	public static final int EVENT_CALL = 1;	
	public static final int EVENT_STOCK = 2;

	int mCategory;
	int mItemId;
	XObject mObj;
	
	public XEvent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public XEvent(int mCategory, int mItemId) {
		super();
		this.mCategory = mCategory;
		this.mItemId = mItemId;
	}

	@Override
	public void saveToDB(Context mContext) {
		// TODO Auto-generated method stub
		switch (mCategory) {
		case EVENT_BUS:
			XBusService xbs = (XBusService)mObj;
			xbs.saveToDB(mContext);
			mItemId = xbs.mId;
			break;
		case EVENT_CALL:
			XCallService xcs = (XCallService)mObj;
			xcs.saveToDB(mContext);
			mItemId = xcs.mId;
			break;
		case EVENT_STOCK:
			XStockService xss = (XStockService)mObj;
			xss.saveToDB(mContext);
			mItemId = xss.mId;
		default:
			return;
		}
		super.saveToDB(mContext);
	}


	@Override
	void setTableName() {
		// TODO Auto-generated method stub
		mTableName = "EVENT";
	}

	@Override
	void setContentValues() {
		// TODO Auto-generated method stub
		mContents = new ContentValues();
		mContents.put("Category", mCategory);
		mContents.put("ItemID", mItemId);
	}

	@Override
	String toJsonString() {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		try {
			obj.put("category", mCategory);
			obj.put("item_id", mItemId);
			obj.put("obj", mObj.toJsonString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj.toString();
	}

	@Override
	void fromJsonString(String jsonString) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(jsonString);
			mId = -1;
			mCategory = obj.getInt("category");
			mItemId = obj.getInt("item_id");
			if(mCategory == EVENT_BUS){
				mObj = new XBusService();				
			}
			else if(mCategory == EVENT_CALL){
				mObj = new XCallService();
			}
			else if(mCategory == EVENT_STOCK){
				mObj = new XStockService();
			}
			mObj.fromJsonString(obj.getString("obj"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
