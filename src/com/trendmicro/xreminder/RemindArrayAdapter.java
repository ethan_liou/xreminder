package com.trendmicro.xreminder;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RemindArrayAdapter extends ArrayAdapter<XRemind> {
	LayoutInflater inflater;
	
	public RemindArrayAdapter(Context context, int textViewResourceId,
			List<XRemind> objects) {
		super(context, textViewResourceId, objects);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return super.getCount();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		RelativeLayout rl = null;
		if(convertView == null)
			rl = (RelativeLayout) inflater.inflate(R.layout.event_cell, null);
		else rl = (RelativeLayout) convertView;
		final TextView tv = (TextView) rl.findViewById(R.id.eventcell_name);
		CheckBox cb = (CheckBox) rl.findViewById(R.id.eventcell_checkbox);
		final XRemind remind = getItem(position);
		tv.setText(remind.mRemindText);
		cb.setChecked(remind.mFinished);
		cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				remind.mFinished = isChecked;
				remind.updateDB(getContext());
				if(remind.mFinished){
					tv.setBackgroundResource(R.drawable.line);
				}
				else{
					tv.setBackgroundColor(Color.TRANSPARENT);
				}
			}
		});
		if(remind.mFinished){
			tv.setBackgroundResource(R.drawable.line);
		}
		else{
			tv.setBackgroundColor(Color.TRANSPARENT);
		}

		return rl;
	}	
}
